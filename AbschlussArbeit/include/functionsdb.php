<?php


session_start();
session_regenerate_id();


function tt($value) {
    echo '<pre>';
    print_r($value);    
    echo '</pre>';

    die("Test beendet");
}


// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

// Fehler checken
function errorCheck($query) 
{
    $errInfo=$query->errorInfo();

    if ($errInfo[0] !== PDO::ERR_NONE) {
        echo $errInfo[2];
        exit();
    }            
    return true;
}


    
// Aufruf der Daten einer Tabelle

function selectAll($table,$params=[])
{

    global $conn;

    
    $sql = "SELECT * FROM $table ";

    if (!empty($params))
    {
        $i=0;
        foreach ($params as $key => $value)
        {
            if(!is_numeric($value))
            {
                $value = "'".$value."'";
            }
            if($i===0)
            {
                $sql = $sql . " WHERE $key=$value";
            }else{
                $sql = $sql . " AND $key=$value";
            }
            $i++;
        }
    }
    
    $query= $conn->prepare($sql);
    $query->execute();
     
   
    
    return $query->fetchAll();

    
}


// Aufruf der Daten eines Strings

function selectOne($table,$params=[])
{

    global $conn;

    $sql = "SELECT * FROM $table ";

    if (!empty($params))
    {
        $i=0;
        foreach ($params as $key => $value)
        {
            if(!is_numeric($value))
            {
                $value = "'".$value."'";
            }
            if($i===0)
            {
                $sql = $sql . " WHERE $key=$value";
            }else{
                $sql = $sql . " AND $key=$value";
            }
            $i++;
        }
    }
  //  $sql = $sql . " LIMIT 1";

    $query= $conn->prepare($sql);
    $query->execute();
          
    return $query->fetch();
}



// Insert Daten in der Tabelle

function insert($table,$params)
{
    global $conn;

    $i=0;
    $col ="";
    $mask="";
    foreach($params as $key => $value)
    {
        if ($i === 0)
        {
            $col = $col . $key;
        $mask = $mask . "'".$value."'";
        $i++;
        }else{
        $col = $col ." ,". $key;
        $mask = $mask ." ,"."'". $value."'";
        }
        $i++;
    }

    $sql = "INSERT INTO $table ($col)
    VALUES ($mask)";


    $query= $conn->prepare($sql);
    $query->execute(); 
    return $conn->lastInsertId();
}


// Update String Function

function update($table,$id,$params)
{
    global $conn;

    $i=0;
    $str ="";
    
    foreach($params as $key => $value)
    {
        if ($i === 0)
        {
        $str= $str . $key ." = '".$value."'";
        
        }else{
            $str= $str .", ". $key . "= " . "'". $value."'";
        
        }
        $i++;
    }

    $sql = "UPDATE $table 
    SET $str
    WHERE id = $id";



    $query= $conn->prepare($sql);
    $query->execute(); 
}


// Löschen des Users

function delete($table,$id)
{
    global $conn;

    $sql = "DELETE FROM $table
            WHERE id = $id";
    
    $query = $conn->prepare($sql);
    $query->execute();
    
}

// Session Starten beim Anmelden oder Registrieren
function startSession($daten)
{

    $_SESSION["enable"] = true;
    $_SESSION["id"] = $daten["id"];
    $_SESSION["login"] = $daten["username"];
    $_SESSION["admin"] = $daten["admin"];
    $_SESSION["email"] = $daten["email"];
            
        if($_SESSION["admin"]){
                header("location:"."adminPostsView");
        }else{
                header("location:"."home");
        }
}

// Aufruf der Daten (posts) mit Autor im Adminpanel

function selectAllFromPostsWithUsers($table1,$table2)
{
    global $conn;
    $sql = "SELECT 
            t1.id,
            t1.id_kategorie,
            t1.post_title,
            t1.img,
            t1.content,
            t1.status,
            t1.created,
            t2.username
            FROM $table1 AS t1 
            JOIN $table2 AS t2 
            ON t1.id_user = t2.id";
    $query = $conn->prepare($sql);
    $query->execute();
    return $query->fetchAll();

}

// Aufruf der Daten für Main Page
function selectAllFromPostsWithUsersbyIndex($table1,$table2,$limit,$offset)
{
    global $conn;
    $sql = "SELECT p. *,u.username FROM $table1
            AS p
            JOIN $table2 AS u
            ON p.id_user = u.id
            WHERE p.status = 1 
            ORDER BY created DESC
            LIMIT $limit OFFSET $offset
            ";
    
    $query = $conn->prepare($sql);
    $query->execute();
    return $query->fetchAll();

}

// Aufruf der Top Kategorien mit Username
function selectTopTopicsbyUsers($table)
{
    global $conn;
    $sql = "SELECT * FROM $table
            WHERE id_kategorie = 18";
    $query = $conn->prepare($sql);
    $query->execute();
    return $query->fetchAll();

}

// Suche nach title und content
function searchInPosts($text,$table1,$table2)
{
    $text = trim(strip_tags(stripcslashes(htmlspecialchars($text))));
    global $conn;
    $sql = "SELECT p. *,u.username 
            FROM $table1 AS p
            JOIN $table2 AS u
            ON p.id_user = u.id
            WHERE p.status = 1
            AND p.post_title LIKE '%$text%' OR p.content LIKE '%$text%'";
    $query = $conn->prepare($sql);
    $query->execute();
    return $query->fetchAll();

}

// Aufruf der Posts mit Autor für Single Page
function selectAllFromPostsWithUserstoSingle($table1,$table2,$id)
{
    global $conn;
    $sql = "SELECT p. *,u.username FROM $table1
            AS p
            JOIN $table2 AS u
            ON p.id_user = u.id
            WHERE p.id = $id ";
    $query = $conn->prepare($sql);
    $query->execute();
    return $query->fetch();

}

// Zaehlen Rows fuer Home page Pagination
function countRow($table)
{
    global $conn;
    $sql = "SELECT COUNT(*) FROM $table WHERE status = 1";
    $query = $conn->prepare($sql);
    $query->execute();
    return $query->fetchColumn();

}