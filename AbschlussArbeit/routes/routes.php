<?php

   use App\Services\Router;

   // Users Routing
   Router::page('/home','home');
   Router::page('/single','single');
   Router::page('/login','login');
   Router::page('/register','register');
   Router::page('/logout','logout');

   if(isset($_SESSION["admin"]))
   {
   //Admin Routing für Posts verwalten
   Router::page('/adminPostsCreate','adminPostsCreate');
   Router::page('/adminPostsView','adminPostsView');
   Router::page('/adminPostsEdit','adminPostsEdit');

   //Admin Routing für Topics verwalten
   Router::page('/adminTopicsCreate','adminTopicsCreate');
   Router::page('/adminTopicsView','adminTopicsView');
   Router::page('/adminTopicsEdit','adminTopicsEdit');

   //Admin Routing für Users verwalten
   Router::page('/adminUsersCreate','adminUsersCreate');
   Router::page('/adminUsersView','adminUsersView');
   Router::page('/adminUsersEdit','adminUsersEdit');

   //Admin Routing für Kommentare verwalten
   Router::page('/adminCommentsEdit','adminCommentsEdit');
   Router::page('/adminCommentsView','adminCommentsView');
   }
   // Users Routing
   Router::page('/search','search');
   Router::page('/kategorie','kategorie');
 
   //Fitness Checker
   Router::page('/fitnessChecker','fitnessChecker');
   //Start der Datenbank
   Router::enable();

?>