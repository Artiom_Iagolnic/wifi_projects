console.log( 'main.js wurde geladen');

    
    //Fitness Checker
document.addEventListener ( 'DOMContentLoaded', function() {

        const alter = document.querySelector( '#alter');
        const groesse = document.querySelector('#groesse');
        const gewicht = document.querySelector( '#gewicht');
        const tail = document.querySelector( '#tail');
        const geschlecht = document.getElementById( 'frau');
        const berechnen = document.querySelector( '#berechnen');

        const result1 = document.querySelector( '#BMI_result');
        const result2 = document.querySelector( '#WHtR_result');
        const result3 = document.querySelector( '#BF_result');

        const userInput = function() {
            const a = Number(alter.value);
            const gr = Number(groesse.value);
            const gw = Number(gewicht.value);
            const t = Number(tail.value);

            if(isNaN(a,gr,gw,t))
            {
                alter.classList.add('error');
                groesse.classList.add('error');
                gewicht.classList.add('error');
                tail.classList.add('error');
            }else{
                showResult(a,gr,gw,t);
            }
        }

        const showResult = function (a,gr,gw,t)
        {
            const bodyMass = Math.floor(gw * 10000 / Math.pow(gr,2));
            const tailUmfang = t/gr;

            if(geschlecht.checked==true )
            {
                sex = 0;
            }else{
                sex = 1;
            }

            const bodyFat = Math.ceil((1.39 * bodyMass) + (0.16 * a) - (10.34 * sex) - 9);

            result1.innerText =  bodyMass;
            result2.innerText =  tailUmfang;
            result3.innerText =  bodyFat;

            const BASE_OFFSET_LEFT = 62;
            const BASE_OFFSET_TOP = 320;
            const PIXEL_PER_UNIT_GEWICHT = 2.9;
            const PIXEL_PER_UNIT_GROESSE = 10;

            const indicator = document.querySelector( '#indicator');

            const showValues = function(gr,gw) {
                const posX = BASE_OFFSET_LEFT + (gr - 160) * PIXEL_PER_UNIT_GROESSE;
                const posY = BASE_OFFSET_TOP - (gw - 40) * PIXEL_PER_UNIT_GEWICHT;

                indicator.style.left = posX + 'px';
                indicator.style.top = posY + 'px';
            }

            showValues( gr,gw);
        }

        berechnen.onclick = userInput;
});


ClassicEditor
    .create( document.querySelector( '#editor' ), {
        toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
        heading: {
            options: [
                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
            ]
        }
    } )
    .catch( error => {
        console.log( error );
    } );
