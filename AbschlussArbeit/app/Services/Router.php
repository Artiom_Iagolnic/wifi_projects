<?php
 
 namespace App\Services;

 class Router 
 {

   
    private static $list = [];

    /*
        Methode registriert neue Routen
    */

    public static function page($uri,$pageName)
    {
        self::$list[] = [
            "uri"=>$uri,
            "page"=>$pageName,
        ];
    }

    
   
    public static function enable()
    {

        
        $query = $_GET['q'];
        

        
        foreach (self::$list as $route)
        {

            if($route["uri"] === '/'.$query)
            {
                require_once "views/pages/" . $route["page"] . ".php";
                die();
            }elseif($query == "")
            {
                header("Location:home");
                die();
            }
        }

         self::error('404');
    }

        public static function error($error)
            {
                require_once "views/errors/".$error.".php";
            }
 }

?>