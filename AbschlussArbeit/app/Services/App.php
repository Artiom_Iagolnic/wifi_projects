<?php

namespace App\Services;
use App\Services\Router;


class App
{
   

    // Starten des Projektes

    public static function start()
    {
        
        self::connect();
        
    }

    // Verbindung zur Datenbank

    public static function connect()
    {
       require_once "include/configdb.php";

        if($enable)
        {
            require_once 'include/dbConnect.php';
            require_once ("include/functionsdb.php");
            
        }else{
            Router::error(503);
        }

        
    }


}


?>