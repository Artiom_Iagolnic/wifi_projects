<?php


$commentsAdmin= selectAll('kommentare');

$page = $_GET["post"];
$email = "";
$comment = "";
$errMsg = [];
$status = 0;
$comments = [];


// Form für Kommentare

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['send-comment'])) 
{

    $email = $_POST["email"];
    $comment = strip_tags(preg_replace("#'#","\'",$_POST["comment"]));
    
    
    $status = isset($_POST['status']) ? 1 : 0;
    

if($email === "" || $comment=== "")
{
    
    array_push($errMsg,"Nicht alle Felder sind ausgefüllt ! <br>");
}elseif(mb_strlen($comment, 'UTF8') < 4)
{
    array_push($errMsg,"Kommentar muss at mindestens 10 Zeichen haben!");
}else{
        $user = selectOne("user",["email" => $email]);
        if($user["email"] === $email)
        {
            $status = 1;
        }

        $comment = [
            "status" => $status,
            "seite" => $page,
            "email" => $email,
            "kommentar" => $comment,
        ];
        
        
        $comment = insert('kommentare',$comment);
        
        $comments= selectAll('kommentare', ['seite' => $page, "status" => 1] );
        
    }
    
}else{
    $email = "";
    $comment = "";
    $comments = selectAll('kommentare', ['seite' => $page, "status" => 1] );

}



?>