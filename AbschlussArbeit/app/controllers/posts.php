<?php


//tt($_SESSION);

if (!($_SESSION)["enable"])
 {
     header("Location: login");
 }



$ok = true;
$errMsg=[];

$id = "";
$title = "";
$content = "";
$topic= "";
$img = "";

$allTopics = selectAll("kategorien");
$posts = selectAll("posts");
$postsAdmin = selectAllFromPostsWithUsers("posts","user");

// Form fügt das Post hinzu
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['add-post'])) 
{
    // Include des Checkens Bildes
    require_once("app/helper/image_hochladen.php");

    $title = trim(preg_replace("#'#","\'", $_POST["title"]));
    $content = trim (preg_replace("#'#","\'",$_POST["content"]));
    $topic = $_POST["topic"];
    
    $publish = isset($_POST['publish']) ? 1 : 0;
    

if($title === "" || $content=== "" || $topic === "")
{
    $ok = false;
    array_push($errMsg,"Nicht alle Felder sind ausgefüllt ! <br>");
}
   

// Wenn alles noch ok ist  
    if($ok ===true)
    {
        $post = [
            "id_user" => $_SESSION['id'],
            "id_kategorie" => $topic,
            "post_title" => $title,
            "content" => $content,
            "img" => $imgName,
            "status" => $publish,
        ];
        
        
        $id = insert('posts',$post);
        $post= selectOne('posts', ['id' => $id] );

        array_push($errMsg,"Kategorie ". "" .$title. "" . " wurde erfolgreich hinzugefügt!");
        
        header("location:adminPostsView");

        
        
      
    }}else{
    $id="";
    $title = "";
    $content = "";
    $publish = "";
    $topic = "";
    
}



//Updaten des Postes

if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['id']))
{
    
    $id =$_GET['id'];
    $onePost= selectOne('posts',["id" => $id]);
    $title = $onePost['post_title'];
    $content = $onePost['content'];
    $topic_id = $onePost['id_kategorie'];
    $img = "uploads/postsImg/".$onePost['img'];

}

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['button-post-update']))
{
        $title = trim(preg_replace("#'#","\'",$_POST["title"]));
        $content = strip_tags(trim(preg_replace("#'#","\'",$_POST["content"])));
        $topic = $_POST["topic"];
        $id_post = $_POST['id_post'];
        $publish = isset($_POST['publish']) ? 1 : 0;
        
        // Include des Checkens Bildes
        require_once("app/helper/image_hochladen.php");

        
        if($title === "" || $content === "" )
        { 
            array_push($errMsg,"Nicht alle Felder sind ausgefüllt !");
        }else{

        $post_array = [
            "id_user" => $_SESSION['id'],
            "id_kategorie" => $topic,
            "post_title" => $title,
            "img" => $_POST['img'],
            "content" => $content, 
            "status" => $publish,
        ];
       
       
        $post = update('posts',$id_post,$post_array);

      

        array_push($errMsg,"Kategorie ". "" .$title. "" . " wurde erfolgreich hinzugefügt!");
        
        header("location:adminPostsView");

    }  
     
}else{
    
    
    
}

if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['pub_id']))
{
    $id = $_GET['pub_id'];
    $publish = $_GET['publish'];

    
    $postId = update('posts',$id,['status' => $publish]);

    header("location:adminPostsView");
    
}

//Löschen des Postes

if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['delete_id']))
{
    $id =$_GET['delete_id'];
    
    delete('posts',$id); 
    header("location:adminPostsView");

}


?>