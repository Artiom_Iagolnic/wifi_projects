<?php



    $ok = true;
    $errMsg = [];


$users = selectAll("user");

 // Register Code
 if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['add-user'])) 
 {

    if(!empty($_POST["admin"]))
    {
        $admin = 1;
    }else{
        $admin = 0;
    }
   
    
     
     $firstname = $_POST["firstname"];
     $secondname = $_POST["secondname"];
     $username=trim($_POST["username"]);
     $email = trim($_POST["email"]);
 
     $pass1 = trim($_POST["pass1"]);
     $pass2 =trim($_POST["pass2"]);


 

 // Prüfen ob alle Felder ausgefüllt sind
 
 if($firstname === "" || $secondname === "" || $username === "" )
 {
     $ok = false;
     
     array_push($errMsg,"Nicht alle Felder sind ausgefüllt ! ");
 }
 
 //Prüfung ob gültige Email

 if(filter_var($email, FILTER_VALIDATE_EMAIL)===false)
 {
     $ok = false;
     
     array_push($errMsg,"Keine gültige Email ! ");
 }else{

         //Prüfung ob Email existiert
             $existence =  selectOne('user',["email" => $email]);  
             if($existence !== false)
             {
                 $ok = false;
                 
                 array_push($errMsg,"Email existiert bereits !");
             };


 }

 // Prüfung ob username existiert

 $existence = selectOne('user',["username" => $username]);
 if($existence !== false)
 {
     $ok = false;
     
     array_push($errMsg,"Username existiert bereits !");
 }

 // prüfung ob Passwort mind 8 Zeichen hat
 if(strlen($pass1) < 8)
 {
     $ok = false;
     
     array_push($errMsg,"Passwort muss 8 Zeichen haben !");
 }

 // Prüfung ob Passwort übereinstimmt
 if($pass1 <> $pass2)
 {
     $ok = false;
     
     array_push($errMsg,"Passwort stimmt nicht! ");
 }

 // Passwort check
 $terms1 = "#[A-Z]+#";
 $terms2 = "#[a-z]+#";
 $terms3 = "#[0-9]+#";

 if( preg_match($terms1,$pass1) &&
     preg_match($terms2,$pass1) &&
     preg_match($terms3,$pass1))
     {

     }else{
         $ok = false;
         
         array_push($errMsg,"Das Passwort braucht Großbuchstabe, Kleinbuchstabe, Zahl ");
     }

 // Wenn alles noch ok ist  
     if($ok ===true)
     {
         
         array_push($errMsg,"User ". "" .$username. "" . " wurde erfolgreich registriert! ");

         $pass1 = password_hash($pass1,PASSWORD_BCRYPT);
         
         $daten = [
             "admin" => $admin,
             "firstname" => $firstname,
             "secondname" => $secondname,
             "username" => $username,
             "email" => $email,
             "password" => $pass1,
         ];
         
         
         $id = insert('user',$daten);
         $user = selectOne('user', ['id' => $id] );

         header("location:adminUsersView");
       
     }
 }else{

     $firstname = "";
     $secondname = "";
     $username="";
     $email = "";
 }

 // Update der User
 if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['edit-id']))
 {
        $id = $_GET['edit-id'];
        $user = selectOne('user', ['id' => $id]);
        $admin = $user["admin"];
        $firstname = $user["firstname"];
        $secondname = $user["secondname"];
        $username=$user["username"];
        $email = $user["email"];
        
 }

 if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['update-user']))
 {
    if(!empty($_POST["admin"]))
    {
        $admin = 1;
    }else{
        $admin = 0;
    }
   
     $id = $_POST['id'];
     $username=trim($_POST["username"]);
     
 
     $pass1 = trim($_POST["pass1"]);
     $pass2 =trim($_POST["pass2"]);

     

  // Prüfen ob alle Felder ausgefüllt sind
  
  if( $username === "" )
  {
      $ok = false;
      array_push($errMsg,"Nicht alle Felder sind ausgefüllt ! ");
  }
  
  // prüfung ob Passwort mind 8 Zeichen hat
  if(strlen($pass1) < 8)
  {
      $ok = false;
      
      array_push($errMsg,"Passwort muss 8 Zeichen haben !");
  }

  // Prüfung ob Passwort übereinstimmt
  if($pass1 <> $pass2)
  {
      $ok = false;
      
      array_push($errMsg,"Passwort stimmt nicht!");
  }

  // Passwort check
  $terms1 = "#[A-Z]+#";
  $terms2 = "#[a-z]+#";
  $terms3 = "#[0-9]+#";

  if( preg_match($terms1,$pass1) &&
      preg_match($terms2,$pass1) &&
      preg_match($terms3,$pass1))
      {

      }else{
          $ok = false;
         
          array_push($errMsg,"Das Passwort braucht Großbuchstabe, Kleinbuchstabe, Zahl <br> ");
      }

  // Wenn alles noch ok ist  
      if($ok ===true)
      {

          array_push($errMsg,"User ". "" .$username. "" . " wurde erfolgreich updatet! <br>");

          $pass1 = password_hash($pass1,PASSWORD_BCRYPT);

          $user = [
              "admin" => $admin,
              "username" => $username,
              "password" => $pass1,
          ];
          
          $user = update('user',$id,$user);
          
          array_push($errMsg,"User ". "" .$username. "" . " wurde erfolgreich updatet! ");
          header("location:adminUsersView");
        
      }
 }



 // Löschen der User
if($_SERVER['REQUEST_METHOD'] === "GET" && isset($_GET["delete-id"]))
{
    $id =$_GET["delete-id"];
    
    delete('user',$id); 
    header("location:adminUsersView");
} 

?>