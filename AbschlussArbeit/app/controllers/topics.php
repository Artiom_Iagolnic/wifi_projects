<?php



$ok = true;
$errMsg = [];

$id = "";
$title = "";
$description = "";

$allTopics = selectAll("kategorien");

// Form fügt die Kategorie hinzu
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['button-topic-create'])) 
{

   
    $title =preg_replace("#'#","\'", $_POST["title"]);
    $description = preg_replace("#'#","\'",$_POST["beschreibung"]);
   

if($title === "" || $description === "" )
{
    $ok = false;
    array_push($errMsg,"Nicht alle Felder sind ausgefüllt !");
}
   


// Prüfung ob Kategorie existiert

$existence = selectOne('kategorien',["title" => $title]);
if($existence !== false)
{
    $ok = false;
    
    array_push($errMsg,"Kategorie existiert bereits !");
}



// Wenn alles noch ok ist  
    if($ok ===true)
    {
        
        
        array_push($errMsg,"Kategorie ". "" .$title. "" . " wurde erfolgreich hinzugefügt! ");

        $topic = [
            "title" => $title,
            "beschreibung" => $description,
           
        ];
        
        $id = insert('kategorien',$topic);
        $topic= selectOne('kategorien', ['id' => $id] );

        
        
        header("location:adminTopicsView");

        
        
      
    }

    
}else{

    $title = "";
    $description = "";
    
}

//Updaten der Kategorie

if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['id']))
{
    

    $id =$_GET['id'];

    $oneTopic = selectOne('kategorien',["id" => $id]);
    $id = $oneTopic['id'];
    $title = $oneTopic['title'];
    $description = $oneTopic['beschreibung'];

}

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['button-topic-update']))
{

    $title = preg_replace("#'#","\'",$_POST["title"]);
    $description = preg_replace("#'#","\'",$_POST["beschreibung"]);
    $id = $_POST['id'];

        if($title === "" || $description === "" )
        {
            $ok = false;
            
            array_push($errMsg, "Nicht alle Felder sind ausgefüllt ! <br>");
        }else{
            
            $topic = [
                "title" => $title,
                "beschreibung" => $description,
            ];
            
            
            $topic_id= update('kategorien',$id,$topic);
    
            header("location:adminTopicsView");
    
        }  
}


//Löschen der Kategorie

if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['delete_id']))
{
    $id =$_GET['delete_id'];
    
    delete('kategorien',$id); 
    header("location:adminTopicsView");

}

?>