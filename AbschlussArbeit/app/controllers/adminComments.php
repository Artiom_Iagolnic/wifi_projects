<?php

$commentsAdmin= selectAll('kommentare');

$email = "";
$comment = "";
$errMsg = [];
$status = 0;
$comments = [];

//Löschen des Kommentars
if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['delete_id']))
{
    $id=$_GET['delete_id'];
    delete("kommentare",$id);

    header("location:adminCommentsView");
}

//Update publish Status
if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['pub_id']))
{
    $id = $_GET['pub_id'];
    $publish = $_GET['publish'];

    
    $commentId = update('kommentare',$id,['status' => $publish]);

    header("location:adminCommentsView");
    exit();
}

//Kommentare updaten
if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['id']))
{
    $comment=selectOne("kommentare",["id"=>$_GET['id']]);

    $id = $comment['id'];
    $text = $comment['kommentar'];
    $user = $comment['email'];
    $publish = $comment['status'];
}

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['button-comment-update'])) 
{
    
    $id = $_POST["id_comment"];
    $text = strip_tags(trim(preg_replace("#'#","\'",$_POST["comment"])));
    $publish = isset($_POST['publish']) ? 1 : 0;

 
        $comment = [
            "kommentar" => $text,
            "status" => $publish,
        ];
        
        
        $comment = update('kommentare',$id,$comment);

        header("location:adminCommentsView");

}else{
    // $id = $_POST["id_comment"];
    // $text = $_POST["comment"];
    // $publish = isset($_POST['publish']) ? 1 : 0;
}


   
?>