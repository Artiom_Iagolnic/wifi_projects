<?php

   


    $ok=true;
    $AGBmessage="";
    $nameMessage="";
    $emailMessage="";
    $usernameMessage="";
    $passMessage="";
    $doneMessage="";

    // Register Code
    if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['button-register'])) 
    {
    

        $admin = 0;
        $firstname = trim($_POST["firstname"]);
        $secondname = trim($_POST["secondname"]);
        $username=trim($_POST["username"]);
        $email = trim($_POST["email"]);
    
        $pass1 = trim($_POST["pass1"]);
        $pass2 =trim($_POST["pass2"]);


        // AGB Zustimmung
    if (isset($_POST["check"]))
    {
        $check = $_POST["check"];
    }else{
        $ok = false;
        $AGBmessage = "Sie müssen AGB zustimmen ! <br>";
    }

    // Prüfen ob alle Felder ausgefüllt sind
    
    if($firstname === "" || $secondname === "" || $username === "" )
    {
        $ok = false;
        $nameMessage = "Nicht alle Felder sind ausgefüllt ! <br>";
    }
    
    //Prüfung ob gültige Email

    if(filter_var($email, FILTER_VALIDATE_EMAIL)===false)
    {
        $ok = false;
        $emailMessage = "Keine gültige Email ! <br>"; 
    }else{

            //Prüfung ob Email existiert
                $existence =  selectOne('user',["email" => $email]);  
                if($existence !== false)
                {
                    $ok = false;
                    $emailMessage .= "Email existiert bereits !";
                };


    }

    // Prüfung ob username existiert

    $existence = selectOne('user',["username" => $username]);
    if($existence !== false)
    {
        $ok = false;
        $usernameMessage = "Username existiert bereits !";
    }

    // prüfung ob Passwort mind 8 Zeichen hat
    if(strlen($pass1) < 8)
    {
        $ok = false;
        $passMessage = "Passwort muss 8 Zeichen haben ! <br>";
    }

    // Prüfung ob Passwort übereinstimmt
    if($pass1 <> $pass2)
    {
        $ok = false;
        $passMessage .= "Passwort stimmt nicht! <br>";
    }

    // Passwort check
    $terms1 = "#[A-Z]+#";
    $terms2 = "#[a-z]+#";
    $terms3 = "#[0-9]+#";

    if( preg_match($terms1,$pass1) &&
        preg_match($terms2,$pass1) &&
        preg_match($terms3,$pass1))
        {

        }else{
            $ok = false;
            $passMessage .= "Das Passwort braucht Großbuchstabe, Kleinbuchstabe, Zahl <br> ";
        }

    // Wenn alles noch ok ist  
        if($ok ===true)
        {
            $doneMessage = "User ". "" .$username. "" . " wurde erfolgreich registriert! <br>";

            $pass1 = password_hash($pass1,PASSWORD_BCRYPT);

            $daten = [
                "admin" => $admin,
                "firstname" => $firstname,
                "secondname" => $secondname,
                "username" => $username,
                "email" => $email,
                "password" => $pass1,
            ];
            
            $id = insert('user',$daten);
            $user = selectOne('user', ['id' => $id] );

            startSession($user);
          
        }
    }else{

        $firstname = "";
        $secondname = "";
        $username="";
        $email = "";
    }


    // Login Code
    if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['button-login']))
    {
        $email = trim($_POST["email"]);
        $pass1 = trim($_POST["pass1"]);

        if($email === "" || $pass1 === "")
        {
            $ok = false;
            $nameMessage = "Nicht alle Felder sind ausgefüllt ! <br>";
        }else{
            $existence =  selectOne('user',["email" => $email]);
           
            if($existence && password_verify($pass1,$existence["password"]))
            {
                startSession($existence);
                

            }else{
    
                $ok = false;
                $emailMessage .= "Email oder Passwort falsch !";
               
            }
        }
    }else{
        $email = "";
    } 


?>