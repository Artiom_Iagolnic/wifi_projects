<?php
    require_once("app/controllers/posts.php");

    use App\Services\Page;

    Page::pagePart("adminHeader");

?>


  <div class="container">
       <div class="row ">

       <?php

    

            Page::pagePart("adminSidebar");

       ?>
       
           <div class="posts col-lg-9 col-12 mt-4 mb-4">
               <div class="button row mb-3">
                   <a href="adminPostsCreate" class="col-4 btn btn-primary">Post hinzufügen</a>
                   <div class="col-1"></div>
                   <a href="adminPostsView" class="col-4 btn btn-warning">Post verwalten</a>
               </div>
               <h2>Manage Posts</h2>
                <div class="row table-title mb-2">
                    <div class="id col-1">ID</div>
                    <div class="title col-3">Title</div> 
                    <div class="author col-3">Autor</div> 
                    <div class=" col-5 manage">Verwalten</div>
                    
                </div>
                <?php foreach ($postsAdmin as $key => $post): ?>
                <div class="row post">
                    <div class="id col-1"><?=$post["id"]?></div>
                    <div class="title col-3">

                  <?php   
                           if(strlen($post['post_title']) > 20)
                            {
                                echo substr($post['post_title'],0,20)."...";
                                 
                            }else{
                                echo $post['post_title'];
                            }
                    ?>
                    </div> 
                    <div class="author col-3"><?=$post["username"]?></div> 
                    <div class="col-1 edit">
                        <a href="adminPostsEdit&id=<?=$post["id"]?>">Edit</a>
                    </div>   
                               
            
                 <div class="col-2 del">
                    <a href="adminPostsEdit&delete_id=<?=$post["id"]?>">Löschen</a>
                </div>         
        
                    
                <?php if(!$post["status"]==0): ?>

                    <div class="status col-1 "><a href="adminPostsEdit&publish=0&pub_id=<?=$post["id"];?>">Unpublish</a></div>
                <?php else: ?>
                    <div class="status col-1 "><a href="adminPostsEdit&publish=1&pub_id=<?=$post["id"];?>">Publish</a></div>
                <?php endif; ?>
                </div>
                <?php endforeach; ?>   
           </div>
       </div>
   </div>


    <!-- Footer -->

     <?php

Page::pagePart("footer");

?>

</body>
</html>