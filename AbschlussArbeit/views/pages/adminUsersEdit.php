<?php

    require_once("app/controllers/users.php");

    use App\Services\Page;

    Page::pagePart("adminHeader");

?>


  <div class="container">
       <div class="row ">

        <?php

            Page::pagePart("adminSidebar");

        ?>

           <div class="posts col-lg-9 col-12 mt-4 mb-4">
               <div class="button row mb-3">
                   <a href="adminUsersCreate" class="col-4 btn btn-primary">User hinzufügen</a>
                   <div class="col-1"></div>
                   <a href="adminUsersView" class="col-4 btn btn-warning">Users verwalten</a>
               </div>
               <h2 >User updaten</h2>

               <?php 
                    require_once("app/helper/errorInfo.php");
               ?>

                <div class="row add-post">
                    <form action="adminUsersEdit" method="post">
                      
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <div class="mb-3 col">
                            <label for="username" class="form-label">Username</label>
                            <input name="username" type="text" class="form-control" id="username" value="<?php echo $username; ?>">
                        </div>
                
                        <div class="mb-3 col">
                            <label for="email" class="form-label">Email address</label>
                            <input  type="email" name="email" class="form-control" id="email" value="<?php echo $email ?>" readonly>
                        
                        </div>
                        
                        <div class="mb-3 col">
                            <label for="password1" class="form-label">Passwort</label>
                            <input name="pass1" type="password" class="form-control" id="password1">
                        </div>
                        
                        <div class="mb-3 col">
                            <label for="password2" class="form-label">Passwort wiederholen</label>
                            <input name="pass2" type="password" class="form-control" id="password2">

                        </div>

                        <div class="col mb-2">
                            
                                <input type="checkbox" name="admin" id="publish" value="1">
                                <label for="publish">Admin ?</label>
                            
                        </div>
                        
                        <div class="col-12">
                            <button name="update-user" class="btn btn-primary" type="submit">User updaten</button>
                        </div>
                    </form>
                </div>

                
           </div>
       </div>
   </div>


     <?php

Page::pagePart("footer");

?>

</body>
</html>