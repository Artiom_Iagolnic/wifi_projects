
<?php

use App\Services\Page;

Page::pagePart('header');

?>

<div class="main-content container mt-4">
    <div class="content row">
        <div class="main-content col-12 col-lg-9">

        <h1>Personal Fitness-Check</h1>
    <div class="row">
            <form class="fitness mb-4 justify-content-center col-md-3 col-12">
                <div class="mb-3">
                    <label for="alter" class="form-label">Ihres Alter</label>
                    <input type="number" class="form-control" id="alter" >
                    
                </div>
                <div class="mb-3">
                    <p>Ihre Geschlecht</p>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="sex" id="man">
                            <label class="form-check-label" for="man">
                                Männlich
                            </label>
                            </div>
                            <div class="form-check">
                            <input class="form-check-input" type="radio" name="sex" id="frau" >
                            <label class="form-check-label" for="frau">
                                Weiblich
                            </label>
                        </div>
                </div>

                <div class="mb-3">
                    <label for="groesse" class="form-label">Ihre Grösse</label>
                    <input type="number" class="form-control" id="groesse" >
                    
                </div>

                <div class="mb-3">
                    <label for="gewicht" class="form-label">Ihre Gewicht</label>
                    <input type="number" class="form-control" id="gewicht" >
                    
                </div>

                <div class="mb-3">
                    <label for="tail" class="form-label">Ihre Tailenumfang</label>
                    <input type="number" class="form-control" id="tail" >
                    
                </div>
              
                <button  type="button" class="btn btn-dark" id="berechnen">Berechnen</button>

            </form>

            <div class="col-md-9 col-12 diagramm">
                
                    <img src="assets/img/1920px-BodyMassIndex.svg.png" class="img" alt="diagramm" id="img_main">
                    <img src="assets/img/kreuzerl.png" alt="ergebniss" class="indicator" id="indicator">
                
            </div>
            </div>

            <div class="row">
                <div class="result">
                    <h3>Ergebnisse</h3>
                    <p >Ihre BMI(Body-Mass-Index): <span id="BMI_result">?</span></p>
                    <p >Ihre WHtR (Taille-zu-Größe-Verhältnis) : <span id="WHtR_result">?</span></p>
                    <p >Ihre Body-Fat : <span id="BF_result">?</span></p>
                </div>
            </div>
            
        </div>
<?php

Page::pagePart('sidebar');

?>
        </div>
    </div>

  <?php

Page::pagePart('footer');

?>

</body>
</html>