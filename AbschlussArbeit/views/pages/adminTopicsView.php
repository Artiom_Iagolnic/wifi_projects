<?php

    require_once("app/controllers/topics.php");
    use App\Services\Page;

    Page::pagePart("adminHeader");

?>

  
  <div class="container">
       <div class="row ">


        <?php

            Page::pagePart("adminSidebar");

        ?>

            <div class="posts col-lg-9 col-12 mt-4 mb-4">
               <div class="button row mb-3">
                   <a href="adminTopicsCreate" class="col-4 btn btn-primary">Kategorie hinzufügen</a>
                   <div class="col-1"></div>
                   <a href="adminTopicsView" class="col-4 btn btn-warning">Kategorie verwalten</a>
                   
               </div>

               <h2>Kategorie verwalten</h2>

               <?php 
                    require_once("app/helper/errorInfo.php");
               ?>
               
                <div class="row table-title mb-2">
                    <div class="id col-1">ID</div>
                    <div class="title col-4">Kategorie Title</div> 
                    <div class=" col-4 manage">Verwalten</div>
                </div>
                    <?php foreach ($allTopics as $key => $oneTopic): ?>
                <div class="row post">
                    <div class="id col-1"><?=$oneTopic["id"];?></div>
                    <div class="title col-4"><?=$oneTopic["title"];?></div> 
                
                <div class="col-2 edit"><a href="adminTopicsEdit&id=<?=$oneTopic["id"];?>">Edit</a></div>
                <div class="col-2 del"><a href="adminTopicsEdit&delete_id=<?=$oneTopic["id"];?>">Löschen</a></div>
                </div>

                    <?php endforeach; ?>
           </div>
       </div>
   </div>


    <!-- Footer -->

     <?php

Page::pagePart("footer");

?>

</body>
</html>