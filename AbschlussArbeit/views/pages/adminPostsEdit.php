<?php
    require_once("app/controllers/posts.php");

    use App\Services\Page;

    Page::pagePart("adminHeader");

?>


  <div class="container">
       <div class="row ">

       <?php

            Page::pagePart("adminSidebar");

        ?>

           <div class="posts col-lg-9 col-12 mt-4 mb-4">
               
               <h2 >Posts updaten</h2>
                <p>
                    <!-- Aufruf des Arrays mit Fehlern -->
                    <?php require_once("app/helper/errorInfo.php");?>

                </p>
                <div class="row add-post">
                    <form action="adminPostsEdit" method="post" enctype="multipart/form-data">
                        <div class="col mb-3">

                        <input type="hidden" name="id_post" value="<?=$id; ?>">

                            <label for="title" class="title">Title</label>
                            <input name="title" type="text" class="form-control" id="title" value="<?php echo $title; ?>">
                            
                        </div>
                        <div class="col mb-3">
                            <label for="content" class="content">Inhalt des Postes</label>
                            <textarea name="content" class="form-control" id="editor" class="form-control"  rows="3" ><?php echo $content; ?></textarea>
                        </div>
                        
                        <div class="input-group col mb-3">
                            
                            <input name="img" type="file" class="form-control" id="upload"  aria-label="Upload" value="<?=$img?>">
                            
                        </div>

                       
                        <select name="topic" class="form-select mb-3" aria-label="Default select example">
                            
                        
                            <?php foreach ($allTopics as $key => $oneTopic): ?>
                               
                            <option value="<?=$oneTopic["id"];?>" <?php if($oneTopic["id"] == $topic_id) echo "selected"; ?> > <?=$oneTopic["title"];?></option>
                                
                           <?php endforeach; ?>
                           
                        </select>
                        <div class="col-6 mb-2">
                            <?php if(empty($publish) && $publish == 0):  ?>
                            <input type="checkbox" name="publish" id="publish" >
                            <label for="publish">
                                Publish
                            </label>
                            <?php else: ?>
                                <input type="checkbox" name="publish" id="publish" checked>
                                <label for="publish">
                                    Publish
                                </label>
                              
                             <?php endif; ?>   
                        </div>


                        <div class="col-6">
                            <button name="button-post-update" class="btn btn-primary" type="submit">Post updaten</button>
                        </div>
                    </form>
                </div>

                
           </div>
       </div>
   </div>


     <?php

Page::pagePart("footer");

?>

</body>
</html>