<?php
    require_once("app/controllers/userAuth.php");

    use App\Services\Page;

    
    Page::pagePart('header');
?>


<body>
<div class="container mt-4 form">
    

        <form  class=" row justify-content-center mb-4" method="post" action="login">
        <h2 class="mb-3 col-12 col-md-4">Login</h2>
        
        <div class="w-100"></div>
            <div class="mb-3 col-12 col-md-4">
                <label for="email" class="form-label">Email address</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" value = "<?php echo $email;?>">
                <p class="error">
                        <?php echo $emailMessage; ?>
                    </p>
            </div>
            <div class="w-100"></div>
                <div class="mb-3 col-12 col-md-4">
                    <label for="password1" class="form-label">Passwort</label>
                    <input name="pass1" type="password" class="form-control" id="password1">

                    <p class="error">
                        <?php echo $nameMessage; ?>
                    </p>
                </div>
            <div class="w-100"></div>

           
            <div class="mb-3 col-12 col-md-4">
                <button name="button-login" type="submit" class="btn btn-secondary">Anmelden</button>
                <p class="mt-3">Sind Sie noch nicht registriert?</p>
                <p>Dann...</p>
                <a href="register" class="btn btn-dark" >Registrieren</a>
            </div>   
        </form>

</div>
<?php

Page::pagePart('footer');

?>
</body>
</html>



