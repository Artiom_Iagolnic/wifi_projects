<?php

require_once("app/controllers/topics.php");


use App\Services\Page;
    

    
Page::pagePart('header');

$posts = selectAll('posts',["id_kategorie" => $_GET["id"],
                            "status" => 1]);    

$topic = selectOne('kategorien',["id" => $_GET["id"]]);


?>

<?php

   

?>



    <!-- Main Content -->
<div class="main-content container mt-4">
    <div class="content row">

        <div class="main-content col-12 col-lg-9">
            <h2>Posts der Kategorie<strong> <?=$topic["title"]?> </strong></h2>

                <?php if(empty($posts)): ?>
                    <h4>In dieser Kategorie gibts es noch keine Beiträge </h4>

                <?php endif; ?>   
                <?php foreach ($posts as $post): ?>

                    <?php
                      $username = selectAllFromPostsWithUserstoSingle('posts','user',$post["id"]);

                      
                    ?>

            <div class="post row">
                <div class=" col-12 col-lg-4">
                    <img class="img-thumbnail" src="uploads/postsImg/<?=$post['img']?>" alt="<?=$post['post_title']?>">
                </div>
                <div class="row col-12 mt-2 col-lg-8">
                    <h5><a href="single&post=<?=$post["id"];?>"> 

                        <?php 
                            if(strlen($post['post_title']) > 100)
                            {
                                echo substr($post['post_title'],0,100)."...";
                                 
                            }else{
                                echo $post['post_title'];
                            }
                           
                        ?>
                        
                    </a></h5>
                        <i class="fas fa-at mb-2"> <?=$username['username']?></i>
                        <i class="fas fa-calendar-day mb-2"> <?=$post['created']?></i>
                    <p class="text">
                    <?php 
                            if(strlen($post['content']) > 100)
                            {
                                echo substr($post['content'],0,200)."...";
                                 
                            }else{
                                echo $post['content'];
                            }
                           
                        ?>
                    </p>
                </div>
            </div>

          <?php endforeach; ?>

          
        </div>

            <?php

                Page::pagePart('sidebar');

            ?>
    </div>
</div>

    <!-- Main Content END -->

    <?php

        Page::pagePart('footer');

    ?>

  </body>
</html>