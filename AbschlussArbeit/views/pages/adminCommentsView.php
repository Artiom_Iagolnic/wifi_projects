<?php
    require_once("app/controllers/adminComments.php");

    use App\Services\Page;

    Page::pagePart("adminHeader");

?>


  <div class="container">
       <div class="row ">

       <?php

    

            Page::pagePart("adminSidebar");

       ?>
       
           <div class="posts col-lg-9 col-12 mt-4 mb-4">
            <div class="col-12 col-md-12 err">
            <p>
                    <!-- Aufruf des Arrays mit Fehlern -->
                    <?php require_once("app/helper/errorInfo.php");?>

                </p>
            </div>
               <h2>Manage Kommentare</h2>
                <div class="row table-title mb-2">
                    <div class="id col-1">ID</div>
                    <div class="title col-3">Text</div> 
                    <div class="author col-3">Autor</div> 
                    <div class=" col-5 manage">Verwalten</div>
                    
                </div>

                <?php foreach ($commentsAdmin as $key => $comment): ?>
                <div class="row post">
                        <div class="id col-1"><?=$comment["id"]?></div>

                        <div class=" col-3">

                            <?php   
                                    if(strlen($comment['kommentar']) > 20)
                                        {
                                            echo substr($comment['kommentar'],0,20)."...";
                                            
                                        }else{
                                            echo $comment['kommentar'];
                                        }
                            ?>
                        </div> 

                        <?php

                            $user=$comment["email"];
                            $user=explode("@",$user);
                            $user=$user[0];


                        ?>

                        <div class="author col-3"><?=$user."@"?></div> 
                        
                    <div class="edit col-1">
                        <a href="adminCommentsEdit&id=<?=$comment["id"]?>">Edit</a>
                    </div>

                    <div class="del col-2">
                        <a href="adminCommentsEdit&delete_id=<?=$comment["id"]?>">Löschen</a>
                    </div>
                        
                    <?php if(!$comment["status"]==0): ?>

                        <div class="status col-1 "><a href="adminCommentsEdit&publish=0&pub_id=<?=$comment["id"];?>">Unpublish</a></div>
                    <?php else: ?>
                        <div class="status col-1 "><a href="adminCommentsEdit&publish=1&pub_id=<?=$comment["id"];?>">Publish</a></div>
                    <?php endif; ?>

                    </div>
                <?php endforeach; ?>   
           </div>
       </div>
   </div>


    <!-- Footer -->

     <?php

Page::pagePart("footer");

?>

</body>
</html>