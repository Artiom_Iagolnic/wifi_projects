<?php
    require_once("app/controllers/posts.php");

    use App\Services\Page;

    Page::pagePart("adminHeader");

?>


  <div class="container">
       <div class="row ">

       <?php


            Page::pagePart("adminSidebar");

        ?>

           <div class="posts col-lg-9 col-12 mt-4 mb-4">
               <div class="button row mb-3">
                   <a href="adminPostsCreate" class="col-4 btn btn-primary">Post hinzufügen</a>
                   <div class="col-1"></div>
                   <a href="adminPostsView" class="col-4 btn btn-warning">Post verwalten</a>
               </div>
               <h2 >Posts hinzufügen</h2>
                <p >

                    <!-- Aufruf des Arrays mit Fehlern -->
                    <?php require_once("app/helper/errorInfo.php");?>

                </p>
                <div class="row add-post">
                    <form action="adminPostsCreate" method="post" enctype="multipart/form-data">
                        <div class="col mb-3">
                            <label for="title" class="title">Title</label>
                            <input name="title" type="text" class="form-control" id="title" value="<?php echo $title; ?>">
                        </div>
                        <div class="col mb-3">
                            <label for="content" class="content">Inhalt des Postes</label>
                            <textarea name="content" class="form-control" id="editor" class="form-control"  rows="3" ><?php echo $content; ?></textarea>
                        </div>
                        <div class="input-group col mb-3">
                            <input name="img" type="file" class="form-control" id="upload"  aria-label="Upload">
                            <button class="btn btn-outline-secondary" type="button" id="upload">Hochladen</button>
                        </div>

                       
                        <select name="topic" class="form-select mb-3" aria-label="Default select example">
                            

                            <?php foreach ($allTopics as $key => $oneTopic): ?>
                            <option value="<?=$oneTopic["id"];?>"><?=$oneTopic["title"];?></option>
                           <?php endforeach; ?>
                            
                        </select>

                        

                        <div class="col-6 mb-2">
                            <input type="checkbox" name="publish" id="publish" value = "1">
                            <label for="publish">Publish</label>
                        </div>


                        <div class="col-6">
                            <button name="add-post" class="btn btn-primary" type="submit">Post hinzufügen</button>
                        </div>
                    </form>
                </div>

                
           </div>
       </div>
   </div>


       <?php

Page::pagePart("footer");

?>
</body>
</html>