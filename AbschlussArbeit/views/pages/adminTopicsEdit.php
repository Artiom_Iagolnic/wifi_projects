<?php
    require_once("app/controllers/topics.php");

    use App\Services\Page;

    Page::pagePart("adminHeader");

?>


  
  <div class="container">
       <div class="row ">

       <?php

            Page::pagePart("adminSidebar");

       ?>

           <div class="posts col-lg-9 col-12 mt-4 mb-4">
               <div class="button row mb-3">
                   <a href="adminTopicsCreate" class="col-4 btn btn-primary">Kategorie hinzufügen</a>
                   <div class="col-1"></div>
                   <a href="adminTopicsView" class="col-4 btn btn-warning">Kategorie verwalten</a>
               </div>
               <h2 >Kategorie updaten</h2>
            
               <p>
                                <?php require_once("app/helper/errorInfo.php");?>
                </p>

                <div class="row add-post">
                    <form action="adminTopicsEdit" method="post" action="adminTopicsEdit">
                        <div class="col mb-3">

                            <input type="hidden" name="id" value="<?php echo $id; ?>" />

                            <label for="title" class="title">Title</label>
                            <input name="title" type="text" class="form-control" id="title" value="<?php echo $title; ?>">
                           
                        </div>
                        <div class="col mb-3">
                            <label for="content" class="content">Kategorie beschreibung</label>
                            <textarea name="beschreibung" class="form-control" id="editor" rows="2"><?php echo $description; ?></textarea>
                            
                            
                        </div>
                        <div class="col-12">
                            <button name="button-topic-update" class="btn btn-primary" type="submit">Kategorie updaten</button>
                        </div>
                    </form>
                </div>

                
           </div>
       </div>
   </div>


     <?php

Page::pagePart("footer");

?>

</body>
</html>