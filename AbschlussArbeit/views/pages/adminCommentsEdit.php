<?php
    require_once("app/controllers/adminComments.php");

    use App\Services\Page;

    Page::pagePart("adminHeader");

?>


  <div class="container">
       <div class="row ">

       <?php

            Page::pagePart("adminSidebar");

        ?>

           <div class="posts col-lg-9 col-12 mt-4 mb-4">
               
               <h2 >Kommentar updaten</h2>
                <p>
                    <!-- Aufruf des Arrays mit Fehlern -->
                    <?php require_once("app/helper/errorInfo.php");?>

                </p>
                <div class="row add-post">
                    <form action="adminCommentsEdit" method="post" >
                        <div class="col mb-3">

                        <input type="hidden" name="id_comment" value="<?=$id; ?>">

                            <label for="user" class="title">User</label>
                            <input name="user" type="text" class="form-control" id="user" value="<?php echo $user; ?>" readonly>
                        </div>
                        <div class="col mb-3">
                            <label for="editor" class="content">Kommentar</label>
                            <textarea name="comment" class="form-control" id="editor" class="form-control"  rows="3" ><?php echo $text; ?></textarea>
                        </div>
                        
                       

                       
                        
                        <div class="col-6 mb-2">
                            <?php if(empty($publish) && $publish == 0):  ?>
                            <input type="checkbox" name="publish" id="publish" >
                            <label for="publish">
                                Publish
                            </label>
                            <?php else: ?>
                                <input type="checkbox" name="publish" id="publish"  checked>
                                <label for="publish">
                                    Publish
                                </label>
                              
                             <?php endif; ?>   
                        </div>


                        <div class="col-6">
                            <button name="button-comment-update" class="btn btn-primary" type="submit">Kommentar updaten</button>
                        </div>
                    </form>
                </div>

                
           </div>
       </div>
   </div>


    <?php

Page::pagePart("footer");

?>

</body>
</html>