<?php

    require_once("app/controllers/users.php");
    use App\Services\Page;

    Page::pagePart("adminHeader");

?>


  <div class="container">
       <div class="row ">

        <?php

            Page::pagePart("adminSidebar");

        ?>

           <div class="posts col-lg-9 col-12 mt-4 mb-4">
               <div class="button row mb-3">
                   <a href="adminUsersCreate" class="col-4 btn btn-primary">User hinzufügen</a>
                   <div class="col-1"></div>
                   <a href="adminUsersView" class="col-4 btn btn-warning">Users Verwalten</a>
               </div>
               <h2>Users Verwalten</h2>
                <div class="row table-title mb-2">
                    <div class="id col-1">ID</div>
                    <div class="role col-1">Role</div>
                    <div class="username col-2">Username</div> 
                    <div class="email col-4">Email</div> 
                    <div class="col-4 manage">Verwalten</div>
                    
                </div>
                <?php foreach($users as $user): ?>
                <div class="row post">
                    <div class="id col-1"><?=$user["id"]?></div>
                    <div class="role col-1">
                        <?php if ($user["admin"] == 0):echo "User"; ?>
                            <?php else: echo "Admin"?>
                        <?php endif; ?>
                    </div> 
                    <div class="author col-2"><?=$user["username"]?></div> 
                    <div class="email col-4"><?=$user["email"]?></div> 
                    <div class="edit col-2 "><a href="adminUsersEdit&edit-id=<?=$user["id"]?>"> Edit</a></div>
                    <div class="del col-2 "><a href="adminUsersView&delete-id=<?=$user["id"]?>">Löschen</a></div>
                </div>

                <?php endforeach; ?>
                
           </div>
       </div>
   </div>


    <!-- Footer -->

     <?php

Page::pagePart("footer");

?>

</body>
</html>