<?php

require_once("app/controllers/topics.php");

use App\Services\Page;
    

    
Page::pagePart('header');
    

 $page = isset($_GET['page']) ? $_GET['page'] : 1;
 $limit = 4;
 $offset = $limit * ($page - 1);
 $total_pages =ceil( countRow('posts') / $limit);


?>

<?php

    $posts = selectAllFromPostsWithUsersbyIndex('posts','user',$limit,$offset);
    
    

    $topTopics = selectTopTopicsbyUsers('posts');

?>

    <!-- Top Themen Block Begin -->
<div class="container mt-4 topics">
        <h3>Top Themen</h3>
            <div class="card-group ">

            <?php foreach($topTopics as $topTopic): ?>
                <div class="card ">
                        <img class="card-img-top img-thumbnail" src="uploads/postsImg/<?=$topTopic['img']?>" alt="<?=$topTopic['post_title']?>">
                    <div class="card-body">
                        <h5 class="card-title"><a class="dark" href="single&post=<?=$topTopic["id"];?>">
                            
                        <?php 
                            if(strlen($topTopic['post_title']) > 50)
                            {
                                echo substr($topTopic['post_title'],0,50)."...";
                                 
                            }else{
                                echo $topTopic['post_title'];
                            }
                           
                        ?>
                        </a></h5>


                        <p class="card-text">
                            
                        <?php 
                            if(strlen($topTopic['content']) > 100)
                            {
                                echo substr($topTopic['content'],0,100)."...";
                                 
                            }else{
                                echo $topTopic['content'];
                            }
                           
                        ?>
                        
                    
                    </p>
                    <div class="card-footer">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </div>
                
                    </div>
                </div>    

            <?php endforeach; ?> 

        </div>       
        
</div>
    <!-- Top Themen Block END -->

    <!-- Main Content -->
<div class="main-content container mt-4">
    <div class="content row">
        <div class="main-content col-12 col-lg-9">
            <h2>Neueste Blogbeiträge </h2>

            <?php foreach ($posts as $post): ?>

            <div class="post row">
                <div class=" col-12 col-lg-4">
                    <img class="img-thumbnail" src="uploads/postsImg/<?=$post['img']?>" alt="<?=$post['post_title']?>">
                </div>
                <div class="row col-12 mt-2 col-lg-8">
                    <h5><a href="single&post=<?=$post["id"];?>"> 

                        <?php 
                            if(strlen($post['post_title']) > 100)
                            {
                                echo substr($post['post_title'],0,100)."...";
                                 
                            }else{
                                echo $post['post_title'];
                            }
                           
                        ?>
                        
                    </a></h5>
                        <i class="fas fa-at mb-2"> <?=$post['username']?></i>
                        <i class="fas fa-calendar-day mb-2"> <?=$post['created']?></i>
                    <p class="text">
                    <?php 
                            if(strlen($post['content']) > 100)
                            {
                                echo substr($post['content'],0,200)."...";
                                 
                            }else{
                                echo $post['content'];
                            }
                           
                        ?>
                    </p>

                </div>
            </div>

          <?php endforeach; ?>

          <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center ">
            <li class="page-item ">
                <a class="btn btn-light " href="home&page=1">Erste</a>
            </li>
          
                <?php if($page > 1): ?>

                    <li class="page-item ">
                        <a class="btn btn-light " href="home&page=<?=$page - 1?>">Vorherige</a>
                    </li>

                <?php endif; ?>

                <?php if($page < $total_pages): ?>

                    <li class="page-item ">
                        <a class="btn btn-light " href="home&page=<?=$page + 1?>">Nächste</a>
                    </li>

                <?php endif; ?>

            <li class="page-item">
                <a class="btn btn-light" href="home&page=<?=$total_pages?>">Lätzte</a>
            </li>
        </ul>
        </nav>
          
        </div>
       

        <?php

            Page::pagePart('sidebar');

        ?>
        
</div>

    <!-- Navigation -->


</div>
    <!-- Main Content END -->

   

    <?php

        Page::pagePart('footer');

    ?>

  </body>
</html>