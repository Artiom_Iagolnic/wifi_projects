<?php

    require_once("app/controllers/userAuth.php");

?>

<?php
    
    use App\Services\Page;

   
    Page::pagePart('header');
    
?>


<body>
<div class="container mt-4 form">
    
        <form class="row row justify-content-center mb-4" method="post" action="register">
            
            <div class="mb-3 col-12 col-md-4 title">
            <p class="registered"><?=$doneMessage?></p>
                <h2 >Registration</h2>
            </div>
            <div class="w-100"></div>

               

            <div class="mb-3 col-12 col-md-4">
                <label for="firstname" class="form-label">Vorname</label>
                <input name="firstname" type="text" class="form-control" id="firstname" value = "<?php echo $firstname;?>">
                
            </div>
            <div class="w-100"></div>
            <div class="mb-3 col-12 col-md-4">
                <label for="secondname" class="form-label">Nachname</label>
                <input name="secondname" type="text" class="form-control" id="secondname" value = "<?php echo $secondname;?>">
                
            </div>
            <div class="w-100"></div>
            <div class="mb-3 col-12 col-md-4">
                <label for="username" class="form-label">Username</label>
                <input name="username" type="text" class="form-control" id="username" value = "<?php echo $username;?>">

                <p class="error">
                        <?php echo $usernameMessage; ?>
                    </p>
                    <p class="error">
                        <?php echo $nameMessage; ?>
                    </p>
                
            </div>
            <div class="w-100"></div>

            

            
            <div class="mb-3 col-12 col-md-4">
                <label for="exampleInputEmail1" class="form-label">Email address</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" value = "<?php echo $email;?>">
                
                <p class="error">
                        <?php echo $emailMessage; ?>
                </p>

            </div>
            <div class="w-100"></div>

            

            <div class="mb-3 col-12 col-md-4">
                <label for="password1" class="form-label">Passwort</label>
                <input name="pass1" type="password" class="form-control" id="password1">
            </div>
            <div class="w-100"></div>
            <div class="mb-3 col-12 col-md-4">
                <label for="password2" class="form-label">Passwort wiederholen</label>
                <input name="pass2" type="password" class="form-control" id="password2">

                <p class="error">
                        <?php echo $passMessage; ?>
                </p>
            </div>
            <div class="w-100"></div>


            <div class="mb-3 form-check col-12 col-md-4">
                <input name="check" type="checkbox" class="form-check-input" id="check">
                <label class="form-check-label" for="check">AGB Zustimmen</label>

                <p class="error">
                        <?php echo $AGBmessage; ?>
                </p>
            </div>
            <div class="w-100"></div>


            <div class="mb-3 col-12 col-md-4">
                <button name="button-register" type="submit" class="btn btn-secondary">Senden</button>
                <p class="mt-3">Sind Sie bereits registriert?</p>
                <p>Dann...</p>
                <a href="login" class="btn btn-dark">Anmelden</a>
            </div>    
        </form>

</div>
<?php

Page::pagePart('footer');

?>
</body>
</html>


