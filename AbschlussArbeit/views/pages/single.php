<?php
    


    use App\Services\Page;

    
    Page::pagePart('header');

    require_once("app/controllers/posts.php");
 
    $post = selectAllFromPostsWithUserstoSingle("posts","user",$_GET["post"]);
    
?>




    <!-- Main Content -->
<div class="main-content container mt-4">
    <div class="content row">
        <div class="main-content col-12 col-lg-9 single-post">
            
            <h2>
                <?=$post["post_title"]?> 
            </h2>
            <div class="single-post row">
                <div class=" col-12 ">
                    <img class="img-thumbnail" src="uploads/postsImg/<?=$post["img"]?>" alt="<?=$post["post_title"]?>">
                </div>
                <div class="row col-12 mt-2 content">
                    
                        <i class="fas fa-at"> <?=$post["username"]?></i>
                        <i class="fas fa-calendar-day"> <?=$post["created"]?></i>
                    <p class="single-text">
                    <?=$post["content"]?>
                    </p>

                   
                </div>
            </div>
            
            <!-- Kommentare -->
            <?php

               Page::pagePart('commentsPart');
            ?>


        </div>

        <?php

            Page::pagePart('sidebar');

        ?>
        
    </div>

</div>


    <!-- Main Content END -->

    <?php

        Page::pagePart('footer');

    ?>

  </body>
</html>