
 <?php

require_once("include/functionsdb.php");

$allTopics = selectAll("kategorien");
?>


<div class="sidebar col-12 col-lg-3 ">
            <div class="search mb-4">
                <h3>Suchen</h3>
                <form action="search" method="POST">
                    <div class="field">
                        <input type="text" name="search" class="search" placeholder="Suchen..." required>
                    </div>
                    <div class="send">
                        <button name="search-button" type="submit" class="search-btn btn btn-secondary ">Suchen</button>
                    </div>
                </form>
            </div>
            
            <div class="category">
                <h3>Kategorien</h3>
                <ul>
                    <?php foreach ($allTopics as $key => $oneTopic):?>
                        <li><a href="kategorie&id=<?=$oneTopic['id'];?>"><?=$oneTopic['title'];?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>



</div>
      