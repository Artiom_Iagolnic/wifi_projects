<?php

 
    require_once("app/controllers/comments.php");
    
    
?>

<div class="col-lg-12 col-12 mb-5">
    <h3>Kommentar hinzufügen</h3>
<!-- Aufruf des Arrays mit Fehlern -->
    <?php require_once("app/helper/errorInfo.php");?>
    <form action="<?="single&post=$page"?>" method="post">
        <input type="hidden" name="page" value="<?=$page?>" >
        <div class="mb-3">
            <label for="email" class="form-label"></label>
            <input name="email" type="email" class="form-control" id="email" value="<?=$_SESSION['email']?>" required>
        </div>
        <div class="mb-3">
            <label for="comment" class="form-label">Schreiben Sie ein Kommentar</label>
            <textarea name="comment" class="form-control" id="comment" rows="3"></textarea>
        </div>
        <div class="col-12">
            <button name="send-comment" type="submit" class="btn btn-dark">Senden</button>
        </div>
    </form>

    <?php if(count($comments) > 0): ?>
        <div class="row all-comments">
            <h3 class=" col-12">Alle Kommentare</h3>
                <?php foreach($comments as $comment): ?>

                    <div class="one-comment col-12">
                    <i class="fas fa-user"></i>   <span><?=$comment["email"]?></span>
                    <i class="fas fa-calendar-alt"></i> <span><?=$comment["created"]?></span>
                        <div class="col-12 text">
                        <?=$comment["kommentar"]?>
                        </div>
                    </div>
                    

                <?php endforeach;?>
        </div>
    <?php endif; ?>
</div>