<?php
    

    require_once("app/controllers/userAuth.php");



?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Mein CSS -->
    <link rel="stylesheet" href="assets/css/adminstyle.css" type="text/css" >

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300&display=swap" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300&family=Poppins:ital,wght@0,200;0,300;1,200;1,600&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/0ea574718a.js" crossorigin="anonymous"></script>

    

    <title>Fitness Blog</title>
  </head>
  <body>
    
<header> 
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark container-fluid">
            <div class="container ">
                <h2><a class="navbar-brand" href="home">Fitness Blog</a></h2>
                    <button class="navbar-toggler " type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon "></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="home">Home</a>
                            </li>
                            
                         

                                <?php if (isset($_SESSION['id'])):?>

                                    <a class="nav-link " href="#"><i class="fas fa-user"></i> 
                                        <?php echo $_SESSION['login'];  ?></a>
                                    </li>
                                    
                                        <li class="nav-item">
                                            <a class="nav-link " href="adminPostsView"><i class="fas fa-laptop-house"></i> Admin Panel</a>
                                        </li>

                                <?php endif; ?>    
                                    <li class="nav-item">
                                        <a class="nav-link " href="logout"><i class="fas fa-laptop-house"></i> Ausloggen</a>
                                    </li>
                                            
                        </ul>
                    </div>
            </div>
        </nav>
       
    </header>