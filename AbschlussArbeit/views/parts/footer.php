    <!-- Footer -->

    <div class="footer container-fluid bg-dark">
    <div class="footer-content container">
        <div class="row">
            <div class="about col-md-4 col-12">
                <h3>Folge mir</h3>
                <div class="contacts">
                    <span><i class="fas fa-phone"> 123-123-543</i></span> <br>
                    <span><i class="fas fa-envelope"> arnold@gmail.com</i></span>
                </div>
                <div class="socials">
                    <a href="http://facebook.com"><i class="fab fa-facebook-square"></i></a>
                    <a href="instagram.com"><i class="fab fa-instagram"></i></a>
                    <a href="twitter.com"><i class="fab fa-twitter"></i></a>
                    <a href="youtube.com"><i class="fab fa-youtube"></i></a>
                </div>
            </div>

            <div class="links col-md-4 col-12">
                <h3>Quick Links</h3>
                <ul>
                    <li><a href="#">Events</a></li>
                    <li><a href="#">Karriere</a></li>
                    <li><a href="">Gallery</a></li>
                    <li><a href="">Impressum | Dateschutz</a></li>
                </ul>
            </div>

            <div class="write col-md-4 col-12">
                <h3>Schreiben Sie uns!</h3>
                <form action="#" method="POST" class="mb-4">
                    <div>
                        <input type="email" name = "email" placeholder="Ihre Email...">
                        <textarea name="text" id="" cols="30" rows="5"></textarea>
                    </div>
                    <div>
                        <button type="submit" class="footer-btn btn btn-light">Senden</button>
                    </div>
                </form>
            </div>
        </div>
    </div>  

</div>


    <!--Footer END --> 

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->

    <!-- My Scripts -->
    <script src="assets/js/main.js"></script>